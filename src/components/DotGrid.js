/*
 * Composant d'une grille de points comme sur la maquette 
 */

import { StyleSheet, View } from "react-native"; 

export default function DotGrid(col, row) {

  return (
    <View style={styles.container}>
      <View style={[styles.dot, {opacity: 0.2}]} />
      <View style={[styles.dot, {opacity: 0.2}]} />
      <View style={[styles.dot, {opacity: 0.2}]} />
      <View style={[styles.dot, {opacity: 0.2}]} />
      <View style={[styles.dot, {opacity: 0.2}]} />
      <View style={[styles.dot, {opacity: 0.2}]} />

      <View style={[styles.dot, {opacity: 0.1}]} />
      <View style={[styles.dot, {opacity: 0.1}]} />
      <View style={[styles.dot, {opacity: 0.1}]} />
      <View style={[styles.dot, {opacity: 0.1}]} />
      <View style={[styles.dot, {opacity: 0.1}]} />
      <View style={[styles.dot, {opacity: 0.1}]} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: 90,
    flexDirection: "row",
    flexWrap: "wrap",
    marginLeft: 90,
    marginTop: -10,
  },
  dot: {
    width: 4,
    height: 4,
    borderRadius: 200,
    backgroundColor: 'white',
    opacity: 0.2,
    margin: 5,
  }
})
