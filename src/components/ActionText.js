/*
 * Implémentation d'un button sous forme de texte
 */

import { View, Text, StyleSheet } from 'react-native';
import { Colors } from '../assets/Colors';

export default function ActionText({ title, onPress}) {
    return (
        <View style={styles.container}>
            <Text onPress={onPress} style={styles.link}>{title}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 20,
    },
    link: {
        color: Colors.foreground,
    },
});