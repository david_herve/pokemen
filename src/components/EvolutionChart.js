/*
 * Composant réalisant la chaine d'evolution d'un pokeman
 */
import { useContext } from 'react';
import {View, Text, StyleSheet, Image, ImageBackground} from 'react-native';
import { Entypo } from '@expo/vector-icons'; 
import PokemanContext from '../assets/Context';
import useEvolutionChain from '../hooks/UseEvolutionChain';
import usePokeman from '../hooks/UsePokeman';

// Creation du composant pokemonIcon
function PokemonIcon({ name }) {

  // Utilisation du hook personnalisé usePokeman pour faire un appel api
  const [pokeman, isLoading] = usePokeman(name);

  // Image de fond
  const pokeball = require('../assets/img/pokeball_grey.png');

  return (
    <View>
      {isLoading ? <Text>Loading</Text> :(
        <ImageBackground source={pokeball} opacity={0.1} style={styles.imageBg} >
          <Image 
            style={styles.icon}
            source={{
              uri: pokeman.sprites.other["official-artwork"].front_default
            }}
          />
          <Text style={{fontWeight: 'bold', marginTop: 18}}>{pokeman.nameFormat}</Text>
          </ImageBackground>
      )}
        </View>
  )
}

// Creation d'une ligne d'evolution
function ChartRow({start, end, level}) {
  // Utilisation du contexte pour avoir la couleur
  const { color }= useContext(PokemanContext);
  return (
    <View style={styles.row}>
      <PokemonIcon name={start.name} />
      <View style={{alignItems: 'center'}}>
        <Entypo name="arrow-long-right" size={60} color={color} />
        <Text>lvl {level}</Text>
      </View>
      <PokemonIcon name={end.name} />
    </View>
  )
}

export default function EvolutionChart({ evolutionUrl }) {

  // Utilisation d'un hook personnalisé useEvolutionChain et du contexte pokeman
  const pokeman = useContext(PokemanContext);
  const [pokemenChain, isLoading] = useEvolutionChain(evolutionUrl);

  const chart = isLoading ? [] : pokemenChain.map(({start, end, level, index}) => 
    <ChartRow key={index.toString()} start={start} end={end} level={level} />
  )

  return (
    <View style={styles.container}>
      <Text style={[styles.h2, {color: pokeman.color}]}>Evolution Chart</Text>
      {isLoading ? <Text>Loading</Text>: (
        <View>
          {chart}
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
  },
  imageBg: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 110,
    height: 110,
    margin: 10,
  },
  icon: {
    width: 100,
    height: 100,
  },
  h2: {
    fontWeight: 'bold',
    fontSize: 24,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 10,
    marginBottom: 10,
    backgroundColor:  'rgba(80, 80, 80, 0.1)',
    borderRadius: 13,
    padding: 10,
  }
})
