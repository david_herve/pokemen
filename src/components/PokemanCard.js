/*
 * Composant d'une carte pokeman
 */

import {StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity} from "react-native";
import {Colors} from '../assets/Colors';
import { lightenColor } from "../assets/Utils";
import { LinearGradient } from "expo-linear-gradient";
import DotGrid from "./DotGrid";
import Type from './Type';
import usePokeman from "../hooks/UsePokeman";

export default function PokemanCard({ navigation, item }) {

  // Utilisation d'un hook personnalisé
  const [pokeman, isLoading] = usePokeman(item.name);

  // Récupération des types du pokemon, vide si c'est en chargement
  const types = isLoading ? [] : pokeman.types.map(({slot, type}) => {
    return (
      <Type key={slot.toString()} type={type.name} />
    )
  });

  // Image de fond
  const pokeball = require('../assets/img/pokeball_white.png');

  // Navigation vers la page du pokemon
  const onPress = () => {
    navigation.navigate("Pokeman", {...pokeman});
  }

  return (
    <View style={styles.container}>
      {isLoading ? <Text>Loading</Text> :(

        <LinearGradient colors={[lightenColor(pokeman.color, 5), lightenColor(pokeman.color, 2)]} style={styles.card}>

          <TouchableOpacity onPress={onPress} style={{width: '100%', height: '100%'}}>
            <View style={styles.imageParent}>
              <Image source={pokeball} opacity={0.1} style={styles.imageBg} />
            </View>
            <DotGrid />

            <View>
              <Text style={styles.id}>{pokeman.idFormat}</Text>
              <Text style={styles.name}>{pokeman.nameFormat}</Text>
              <View style={styles.types}>
                {types}
              </View>
            </View>

            <Image 
              style={styles.image}
              source={{
                uri: pokeman.sprites.other["official-artwork"].front_default
              }}
            />
          </TouchableOpacity>
        </LinearGradient>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 8,
    padding: 5,
  },
  card: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: '100%',
    height: 100,
    padding: 10,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  imageBg: {
    width: 150,
    height: 150,
    position: 'absolute',
    top: -25,
    left: 20,
  },
  id: {
    color: Colors.black,
    fontWeight: '500',
  },
  name: {
    color: Colors.white,
    fontSize: 25,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  types: {
    flexDirection: 'row',
  },
  image: {
    width: 110,
    height: 110,
    position: 'absolute',
    bottom: 1,
    right: 5,
  },
  imageParent: {
    width: 136, 
    height: 100, 
    top: -10,
    position: 'absolute',
    left: 210,
    overflow: 'hidden',
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  }
})

