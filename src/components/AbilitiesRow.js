/*
 * Composant pour la ligne des abilités du pokeman
 */
import { View, Text, StyleSheet } from 'react-native';
import { nameFormat } from '../assets/Utils';

export default function AbilitiesRow({data}) {

  // Récupération des types du pokemon, vide si c'est en chargement
  const abilities = data.map(({slot, ability, is_hidden}) => {
    return (
      <View key={slot.toString()}>
        <View style={{flexDirection: 'row'}}>
          <Text>{slot}. </Text>
          <Text>{nameFormat(ability.name)} {is_hidden ? "(hidden ability)": ""}</Text>
        </View>
      </View>
    )
  });

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Abilities</Text>
      <View style={styles.abilities}>
        {abilities}
      </View>
    </View>
  )

}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
  },
  label: {
    width: 150,
    fontWeight: 'bold',
  },
  abilities: {
    flexDirection: 'column',
  }
})
