/*
 * Composant implémentant les icons pour les types du pokeman
 */

import {StyleSheet} from 'react-native';
import {Colors} from '../assets/Colors';
import {MaterialCommunityIcons, MaterialIcons, Ionicons, FontAwesome, FontAwesome5, Entypo} from '@expo/vector-icons';

export default function Type(attributs) {
  const {icon, lib} = attributs;
  switch(lib) {
    case "ionicons":
      return (
        <Ionicons style={styles.icon} name={icon} size={15} color={Colors.white}/>
      );
    case "fontawesome":
      return (
        <FontAwesome style={styles.icon} name={icon} size={15} color={Colors.white}/>
      );
    case "fontawesome5":
      return (
        <FontAwesome5 style={styles.icon} name={icon} size={15} color={Colors.white}/>
      );
    case "entypo":
      return (
        <Entypo style={styles.icon} name={icon} size={15} color={Colors.white}/>
      );
    case "materialcommunityicons":
      return (
        <MaterialCommunityIcons style={styles.icon} name={icon} size={15} color={Colors.white}/>
      );
    case "materialicons":
      return (
        <MaterialIcons style={styles.icon} name={icon} size={15} color={Colors.white}/>
      );
    default:
      return (
        <Ionicons style={styles.icon} name="alert-circle" size={15} color={Colors.white}/>
      );
  }
};

const styles = StyleSheet.create({
  icon: {
    marginRight: 4,
    marginTop: 2,
  },
})
