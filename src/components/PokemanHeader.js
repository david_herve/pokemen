/*
 * Composant d'un header pour faire le screen d'un pokeman
 */
import React, { useContext } from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { NavigationContext } from '@react-navigation/native';
import { LinearGradient } from "expo-linear-gradient";
import { Ionicons } from '@expo/vector-icons'; 
import PokemanContext from "../assets/Context";
import { darkenColor } from "../assets/Utils";
import { Colors } from "../assets/Colors";
import Type from '../components/Type';

export default function PokemanHeader() {
  const pokeman = useContext(PokemanContext);
  const navigation = React.useContext(NavigationContext);

  // Récupération des types du pokemon
  const types = pokeman.types.map(({slot, type}) => {
    return (
      <Type key={slot.toString()} type={type.name} />
    )
  });

  return (
    <LinearGradient 
      colors={[
        pokeman.color,
        darkenColor(pokeman.color, 13),
      ]}
      style={styles.container}
    >
      <Ionicons style={styles.back} name="arrow-back" size={33} color="white" onPress={() => navigation.goBack()} />
      <Image 
        style={styles.image}
        source={{
          uri: pokeman.sprites.other["official-artwork"].front_default
        }}
      />
      <View style={styles.attributs}>
        <Text style={styles.id}>{pokeman.idFormat}</Text>
        <Text style={styles.name}>{pokeman.nameFormat}</Text>
        <View style={{flexDirection: 'row'}} >
          {types}
        </View>
      </View>
    </LinearGradient>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: 240,
    paddingTop: 80,
  },
  back: {
    position: 'absolute',
    top: 50,
    left: 10,
    bottom: 0,
  },
  image: {
    width: 140,
    height: 140,
  },
  attributs: {
    marginLeft: 28,
  },
  id: {
    color: Colors.black,
    fontWeight: '500',
  },
  name: {
    color: Colors.white,
    fontSize: 25,
    fontWeight: 'bold',
    marginBottom: 5,
  },
});
