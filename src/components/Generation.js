/*
 * Composant pour le filtre d'une generation pokemon
 */
import { useState, useEffect } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import {nameFormat} from '../assets/Utils';
import useGeneration from '../hooks/UseGeneration';
import usePokeman from '../hooks/UsePokeman';
import {Colors} from '../assets/Colors';
import AsyncStorage from '@react-native-async-storage/async-storage';

// Fonction pour un nom de genration formatté : Generation I
function formatGeneration(name) {
  const words = name.split("-");
  return nameFormat(words[0]) + " " + words[1].toUpperCase();
}

// Composant pour afficher un pokemon en icon
const PokemanIcon = ({ name }) => {
  const [pokeman, isLoading] = usePokeman(name);

  if(isLoading) return <Text>Loading</Text>;

  return (
    <Image 
      key={pokeman.id}
      style={styles.icon}
      source={{
        uri: pokeman.sprites.other["official-artwork"].front_default
      }}
    />
  );
}

// AsyncStorage pour @generation
const storeGenerations = async(list) => {
  try {
    await AsyncStorage.setItem('@generations', JSON.stringify(list));
  } catch (e) {
    console.error(e);
  }
}

const getGenerations = async() => {
  try {
    return await AsyncStorage.getItem('@generations');
  } catch(e) {
    console.error(e);
  }
}

export default function Generation({ name, url }) {

  // Etats du composants
  const [generation, isLoading] = useGeneration(url);
  const [generations, setGenerations] = useState([]);
  const [isClick, setClick] = useState(false);


  // Liste des icons de pokemons
  const pokemons = isLoading ? [] : generation.map(({name}) => {
    return <PokemanIcon key={name} name={name} />
  });

  // OnClick pour l'ajout dans le local storage
  const onClick = async () => {
    setClick(!isClick);
    if(!isClick) {
      storeGenerations([...generations, name]);
    } else {
      storeGenerations(generations.filter(e => e !== name));
    }
  }

  // Image de fond
  const pokeball = require('../assets/img/pokeball_white.png');

  // Chargement automatique dans le localStorage du @generations
  useEffect(() => {
    let isCancelled = false;

    getGenerations().then((list) => {
      if (!isCancelled && list !== null) {
        const gen = JSON.parse(list);
        setGenerations(gen);

        // SetClick si deja allume
        setClick(gen.includes(name));
      }
    });

    return () => isCancelled = true;
  }, []);

  return (
    <TouchableOpacity style={[styles.container, {backgroundColor: !isClick ? "#e5e3e3": "#EA5D60"}]} onPress={onClick}>
      <View style={styles.imageContainer}>
        <Image source={pokeball} opacity={0.2} style={styles.imageBg} />
      </View>
      <View style={{flexDirection: 'row'}}>
        {pokemons}
      </View>
      <Text style={[styles.title, {color: !isClick? Colors.black : Colors.white}]}>{ formatGeneration(name) }</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    margin: 5,
    height: 120,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.34,
    elevation: 5,
  },
  title: {
    marginTop: 20,
  },
  icon: {
    width: 42,
    height: 42,
  },
  imageContainer: {
    position: 'absolute',
    top : 60,
    bottom: 0,
    right: 0,
    left: 80,
    width:81,
    height: 60,
    borderBottomRightRadius: 10,
    overflow: 'hidden',
  },
  imageBg: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    width: 100,
    height: 100,
  },
  dotGrid: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
})
