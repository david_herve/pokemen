/*
 * Composant implementant le scroll du pokedex par une flatList
 */

import React from 'react';
import { StyleSheet, View, Text, Animated } from 'react-native';
import usePokeApiQuery from '../hooks/UsePokeApiQuery';
import PokemanCard from './PokemanCard';


export default function PokemenScrollV2({ navigation, search, onScroll, onMomentumScrollBegin, onMomentumScrollEnd, onScrollEndDrag, contentContainerStyle, scrollEventThrottle}){

  // State pour la requete
  const { loading, refetch, data, onLoadMore } = usePokeApiQuery(search); 

  // Id unique pour la carte PokemanCard
  const keyExtractor = (item) => {
    return item.id.toString();
  };

  // Mémoization de PokemanCard (optimization)
  const MemoizedCard = React.memo(PokemanCard);
  const renderItem = ({item}) => <MemoizedCard item={item} navigation={navigation} />;

  return (
    <View style={styles.container} >
      {loading ? <Text>Loading</Text> : (
        <Animated.FlatList
          data={data.pokemen || []}
          keyExtractor={keyExtractor}
          renderItem={renderItem}
          onEndReachedThreshold={0.5}
          refreshing={loading}
          onRefresh={refetch}
          contentContainerStyle={contentContainerStyle}
          onScroll={onScroll}
          onMomentumScrollBegin={onMomentumScrollBegin}
          onMomentumScrollEnd={onMomentumScrollEnd}
          onScrollEndDrag={onScrollEndDrag}
          scrollEventThrottle={scrollEventThrottle}
          initialNumToRender={7}
        />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    width: '100%',
    marginTop: 10,
  },
  contentContainerStyle: {
  }
});
