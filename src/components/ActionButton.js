/*
 * Implémentation d'un bouton selon la maquette
 */

import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Colors } from '../assets/Colors';

export default function ActionButton({ title, onPress }) {
    return (
        <View style={styles.container}>
            <TouchableOpacity
                style={styles.button}
                activeOpacity={0.6}
                onPress={onPress}
            >
                <Text style={styles.title}>{title}</Text>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
  container: {
    margin: 15,
  },
  button: {
    backgroundColor: Colors.foreground,
    borderRadius: 15,
    paddingLeft: 50,
    paddingRight: 50,
    paddingTop: 20,
    paddingBottom: 20,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 5,
  },
  title: {
    fontWeight: 'bold',
    textAlign: 'center',

  }
});
