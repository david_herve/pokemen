/*
 * Implémentation de l'item du carrousel
 */

import React from 'react';
import {StyleSheet, Text, View, Image, useWindowDimensions} from 'react-native';
import {Colors} from '../assets/Colors';

export default function CarrouselItem({ item }) {
    const { width } = useWindowDimensions();

    return (
        <View style={[styles.container, { width }]}>
            <Image source={item.image} style={[ styles.image, { width } ]} />
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description}>{item.description}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        flex: 0.8,
        aspectRatio: 0.7,
        justifyContent: 'center',
        resizeMode: 'contain',
    },
    title: {
        fontWeight: 'bold',
        fontSize: 36,
        marginBottom: 10,
        color: Colors.black,
        textAlign: 'center'
    },
    description: {
        fontWeight: '300',
        fontSize: 20,
        marginBottom: 10,
        color: Colors.softGrey, 
        textAlign: 'center',
        paddingHorizontal: 64,
    },
});
