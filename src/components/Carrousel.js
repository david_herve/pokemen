/*
 * Implémentation du carrousel du screen 'Welcome'
 */

import { useState, useRef } from 'react';
import { View, StyleSheet, FlatList, Animated } from 'react-native';
import CarrouselItem from './CarrouselItem';
import Paginator from './Paginator';
import ActionButton from './ActionButton';

export default function Carrousel({ navigation, slides }) {
    // Paramètres de gestion du carrousel
    const [currentIndex, setCurrentIndex] = useState(0);
    const scrollX = useRef(new Animated.Value(0)).current;
    const slidesRef = useRef(null);

    // Changement d'état des items du carrousel
    const viewableItemsChanged = useRef(({ viewableItems }) => {
        setCurrentIndex(viewableItems[0].index);
    }).current;
    const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current;

    // Attributs pour un bouton dynamique
    const titleActionBtn = currentIndex < slides.length - 1 ? "Next" : "Sign Up";
    const scrollTo = () => {
        if(currentIndex < slides.length - 1) {
            slidesRef.current.scrollToIndex({index: currentIndex + 1});
        } else {
            navigation.navigate('SignUp');
        }
    }

    return (
        <View style={styles.container}>
            <FlatList 
                data={slides}
                renderItem={({ item }) => <CarrouselItem item={item}/> }
                horizontal
                showsHorizontalScrollIndicator={false}
                pagingEnabled
                bounces={false}
                keyExtractor={(item) => item.id}
                onScroll={Animated.event([{ nativeEvent: { contentOffset: { x: scrollX } } }], {
                    useNativeDriver: false,
                })}
                scrollEventThrottle={32}
                onViewableItemsChanged={viewableItemsChanged}
                viewabilityConfig={viewConfig}
                ref={slidesRef}
            />
            <Paginator slides={slides} scrollX={scrollX}/>
            <ActionButton title={titleActionBtn} onPress={scrollTo} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'center',
        alignItems: 'center',
    },
});
