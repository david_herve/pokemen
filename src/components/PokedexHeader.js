/*
 * Implementation du header pour le pokedex avec sa barre de recherche
 */
import React, { createRef, useContext } from "react";
import { NavigationContext } from "@react-navigation/native";
import { View, Text, StyleSheet, TouchableOpacity, Animated } from 'react-native';
import { FontAwesome, Fontisto, Ionicons } from '@expo/vector-icons'; 
import { Colors } from "../assets/Colors";
import SearchBar from './SearchBar';


export default function PokedexHeader({ search, onChangeText }) {

  const inputRef = createRef();
  const navigation = React.useContext(NavigationContext);

  return (
    <View style={styles.container}>
        <View style={styles.filter}>
          <TouchableOpacity style={styles.icon} onPress={() => navigation.navigate("Generations")}>
            <Fontisto name="nav-icon-grid-a" size={20} color={Colors.black} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.icon} onPress={() => navigation.navigate("Filter")}>
            <FontAwesome name="sliders" size={20} color={Colors.black} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.icon} onPress={() => navigation.navigate("Settings")}>
            <Ionicons name="ios-settings-sharp" size={20} color={Colors.black} />
          </TouchableOpacity>
        </View>

        <Text style={styles.title}>
          Pokédex
        </Text>
        <Text style={styles.subtitle}>
          Search for Pokémon by name or using the National Pokédex number (ex: 001).
        </Text>

        <SearchBar  
          value={search}
          placeholder="What Pokémon are you looking for?"
          minLength={3}
          inputRef={inputRef}
          onChangeText={onChangeText}
          delayTimeout={600}
          style={styles.search}
        />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: "#efefef",
    padding: 7,
    paddingTop: 50,
  },
  filter: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  icon: {
    marginLeft: 14,
    marginRight: 14,
  },
  title: {
    color: Colors.black,
    fontWeight: 'bold',
    fontSize: 38,
    margin: 8,
  },
  subtitle: {
    color: Colors.softGrey,
    margin: 8,
    fontSize: 18,
  },
  search: {
    margin: 10, 
    height: 40, 
  },
})
