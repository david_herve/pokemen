/*
 * Implémentation d'une entrée pour un formulaire avec une icon
 */

import { View, StyleSheet, TextInput } from "react-native";
import { Colors } from "../assets/Colors";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export default function Input(props) {
  return (
    <View style={styles.container}>
      <MaterialCommunityIcons style={styles.icon} name={props.icon} size={22} color={Colors.softGrey} />   
      <TextInput style={styles.input} {...props} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderRadius: 15,

    paddingLeft: 50,
    paddingRight: 50,
    paddingTop: 20,
    paddingBottom: 20,

    borderColor: Colors.softGrey,
    margin: 10,

    flexDirection: 'row',
    padding: 15,
  },
  icon: {
    marginTop: 3,
    marginLeft: -35,
  },
  input: {
    flex: 1,
    fontSize: 18,
    marginLeft: 10,
  },
});
