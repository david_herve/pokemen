/*
 * Composant implementant le scroll du pokedex
 */

import { Component } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { RecyclerListView, DataProvider, LayoutProvider } from 'recyclerlistview';
import PokemanCard from './PokemanCard';

// Syntaxe composant due a l'implementation de recyclerlistview
export default class PokemenScroll extends Component {

  constructor(props) {
    super(props);
    this.state =  {
      navigation: props.navigation,
      dataProvider: new DataProvider((r1, r2) => r1 !== r2),
      pokemen: [],
      urlNext: props.url,
      urlPrevious: '',
    }
  }

  // Définition de la clée et de la taille d'un élément du scroll 
  layoutProvider = new LayoutProvider((index) => {
    return index;
  }, (_, dim) => {
    dim.width = Dimensions.get('window').width; 
    dim.height = 125;
  })

  // Définition du rendu unique d'un élément du scroll
  rowRenderer = (key, item) => {
    return (
      <PokemanCard item={item} key={key} navigation={this.state.navigation} />
    ); 
  }

  // définition d'une fonction asynchrone récupérant les données de l'api
  fetchdata = async() => {
    try {
      // appel de l'api 
      const response = await fetch(this.state.urlNext);
      const json = await response.json();

      // modification de l'état du composant
      this.setstate({
        dataProvider: this.state.dataProvider.clonewithrows([...this.state.pokemen, ...json.results]),
        pokemen: [...this.state.pokemen, ...json.results],
        urlNext: json.next,
        urlPrevious: json.previous,
      })

    } catch (error) {
      console.error(error);
    }
  }

  // A la desctruction du composant supprimer les états comme la liste de pokemons
  componentWillUnmount() {
    this.setState({
      dataProvider: new DataProvider((r1, r2) => r1 !== r2),
      pokemen: [],
      urlNext: '',
      urlPrevious: '',
    })
  }

  // Définition d'un appel asynchrone à la fin du scroll pour faire un chargement 
  onHandleEndReached = async() => {
    await this.fetchData();
  }

  render() {
    return (
      <RecyclerListView
        style={styles.container}
        dataProvider={this.state.dataProvider}
        layoutProvider={this.layoutProvider}
        rowRenderer={this.rowRenderer}
        onEndReached={this.onHandleEndReached}
        onEndReachedThreshold={0.5}
      />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    width: '100%',
  },
});
