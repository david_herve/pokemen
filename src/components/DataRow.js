/*
 * Implementation des lignes de données
 */
import {View, Text, StyleSheet} from 'react-native';
import { nameFormat } from '../assets/Utils';

export default function DataRow({label, data}) {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{nameFormat(label)}</Text>
      <Text>{data}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 10,
    marginBottom: 10,
  },
  label: {
    width: 150,
    fontWeight: 'bold',
  }
})
