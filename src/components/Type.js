/*
 * Composant implémentant le type dans le carrousel
 */

import {View, Text, StyleSheet} from 'react-native';
import {Colors} from '../assets/Colors';
import {getColorAndIconByType} from '../assets/Utils';
import IconType from './IconType';

export default function Type({ type }) {
  const {icon, color, lib} = getColorAndIconByType(type);
  return (
    <View style={[styles.container, {backgroundColor: color}]}>
      <IconType icon={icon} lib={lib} />
      <Text style={styles.text}>{ type }</Text>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    textAlign: 'center',
    paddingRight: 5,
    paddingLeft: 5,
    paddingBottom: 2,
    marginRight: 5,
    borderRadius: 4,
    flexDirection: 'row',
  },
  text: {
    fontSize: 11,
    color: Colors.white,
    marginTop: 1,
  }
})

