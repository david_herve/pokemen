/*
 * Implémentation d'une search bar 
 */

import React from "react";
import { View, TextInput, StyleSheet } from "react-native";
import { FontAwesome } from '@expo/vector-icons'; 
import { Colors } from "../assets/Colors";

export default class SearchBar extends React.PureComponent {
  static defaultProps = {
    delayTimeout: 600,
    minLength: 3,
    onChangeText: undefined,
    value: undefined,
    inputRef: undefined
  };

  timerId = null;

  constructor(props) {
    super(props);

    this.state = {
      value: props.value || ""
    };
  }

  resetTimer() {
    clearTimeout(this.timerId);
  }

  componentWillUnmount() {
    this.resetTimer();
  }

  notify = value => {
    const { onChangeText, minLength } = this.props;

    const valueToUpdate = value.length >= minLength ? value : "";

    onChangeText(valueToUpdate);
  };

  runTimeoutUpdate = value => {
    const { delayTimeout } = this.props;

    this.resetTimer();
    this.timerId = setTimeout(() => this.notify(value), delayTimeout);
  };

  onChangeText = value => {
    const { minLength } = this.props;

    const valueToUpdate = value.length >= minLength ? value : "";

    this.setState({ value }, () => this.runTimeoutUpdate(valueToUpdate));
  };

  onBlur = () => {
    this.resetTimer();
    this.notify(this.state.value);
  };

  render() {
    const { inputRef, placeholder, style } = this.props;
    const { value } = this.state;

    return (
      <View style={[styles.container, style]}>
        <FontAwesome name="search" size={20} color={Colors.black} />
        <TextInput
          onChangeText={this.onChangeText}
          onBlur={this.onBlur}
          value={value}
          ref={inputRef}
          placeholder={placeholder}
          style={{marginRight: 40,}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#e5e3e3",
    borderRadius: 6,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
})
