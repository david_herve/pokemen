/*
 * Screen conteneur pour les pages Pokedex, Region et Profile
 */

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Colors} from '../assets/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import PokedexScreen from './Pokedex';
import ProfileScreen from './Profile';
import RegionScreen from './Region';

const Tab = createBottomTabNavigator();

export default function StartScreen() {
  return (
      <Tab.Navigator 
          initialRouteName="Pokedex"
          scrollEnabled='true'
          screenOptions={{
            headerShown: false,
            tabBarActiveTintColor: Colors.foreground,
          }}
      >
        <Tab.Screen
          name="Region"
          component={RegionScreen}
          options={{
            tabBarIcon: ({size, focused}) => (
              <MaterialCommunityIcons name="map-marker" size={size} color={focused ? Colors.foreground : Colors.softGrey} />   
            ),
          }}
        />
        <Tab.Screen 
          name="Pokedex"
          component={PokedexScreen} 
          options={{
            tabBarIcon: ({size, focused}) => (
              <MaterialCommunityIcons name="notebook" size={size} color={focused ? Colors.foreground : Colors.softGrey} />   
            ),
          }}
        />
        <Tab.Screen
          name="Profile" 
          component={ProfileScreen}
          options={{
            tabBarIcon: ({size, focused}) => (
              <MaterialCommunityIcons name="card-account-details" size={size} color={focused ? Colors.foreground : Colors.softGrey} />   
            ),
          }}
        />
      </Tab.Navigator>
  );
}
