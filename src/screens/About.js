/*
 * About page pour la présentation du pokemon
 */

import { useContext } from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import PokemanContext from '../assets/Context';
import AbilitiesRow from '../components/AbilitiesRow';
import DataRow from '../components/DataRow';
import usePokemanDescription from '../hooks/UsePokemanDescription';

export default function AboutScreen() {
  const pokeman = useContext(PokemanContext);
  const [description, isLoading] = usePokemanDescription(pokeman.id);

  return (
    <View style={styles.container}>
      {isLoading ? <ActivityIndicator /> : (
        <View>
          <Text style={{marginBottom: 20}}>{description.replace(/\r?\n|\r/g, " ")}</Text>
          <Text style={[styles.h2, {color: pokeman.color}]}>Pokédex Data</Text>
          <View>
            <DataRow label="Height" data={`${pokeman.height /10} m`} />
            <DataRow label="Weight" data={`${pokeman.weight / 10} kg`} />
            <AbilitiesRow data={pokeman.abilities} />
          </View>
          <Text style={[styles.h2, {color: pokeman.color}]}>Training</Text>
          <View>
            <DataRow label="Catch Rate" data={pokeman.stats[0].base_stat} />
            <DataRow label="Base Exp" data={pokeman.base_experience} />
          </View>
        </View>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  h2: {
    fontWeight: 'bold',
    fontSize: 24,
  }
})
