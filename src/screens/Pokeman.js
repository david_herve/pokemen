/*
 * Screen pour la présentation d'un pokeman
 */

import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import PokemanContext from '../assets/Context';
import AboutScreen from './About';
import StatsScreen from './Stats';
import EvolutionScreen from './Evolution';
import PokemanHeader from '../components/PokemanHeader';

const Tab = createMaterialTopTabNavigator();

export default function PokemanScreen({route}) {
  return (
    <PokemanContext.Provider value={route.params} >
      <PokemanHeader />
      <Tab.Navigator>
        <Tab.Screen 
          name="About" 
          component={AboutScreen}
        />
        <Tab.Screen name="Stats" component={StatsScreen} />
        <Tab.Screen name="Evolution" component={EvolutionScreen} />
      </Tab.Navigator>
    </PokemanContext.Provider>
  )
}
