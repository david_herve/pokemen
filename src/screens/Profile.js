/*
 * Screen du profil utilisateur pour changer ses propres données
 */

import { View, Text } from 'react-native';

export default function ProfileScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>Profile</Text>
        </View>
    );
}
