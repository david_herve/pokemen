/*
 * Screen pour les statistiques d'un pokemon
 */
import { useContext } from 'react';
import {View, Text, StyleSheet} from 'react-native';
import PokemanContext from '../assets/Context';
import DataRow from '../components/DataRow';

export default function StatsScreen() {
  const pokeman = useContext(PokemanContext);

  // Liste de données
  const stats = pokeman.stats.map(({base_stat, stat}) => {
    return (
      <DataRow key={stat.name} label={stat.name} data={base_stat} />
    )
  });

  return (
    <View style={styles.container}>
        <Text style={[styles.h2, {color: pokeman.color}]}>Base Stats</Text>
        <View>
          {stats}
        </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  h2: {
    fontWeight: 'bold',
    fontSize: 24,
  }
})
