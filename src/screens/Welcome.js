/*
 * Screen de Bienvenue de l'application
 */

import { View, StyleSheet, ImageBackground } from 'react-native';
import ActionText from '../components/ActionText';
import Carrousel from '../components/Carrousel';

// Elements à présenter dans le carrousel
const slides = [
    {
        id: '1',
        title: 'Welcome to Pokemen',
        description: 'All men in your pocket',
        image: require('../assets/img/pikachu.png'),
    },
    {
        id: '2',
        title: 'Complete database',
        description: 'You can find any Pokeman and information about it',
        image: require('../assets/img/carapuce.png'),
    },
    {
        id: '3',
        title: 'Habitat locations',
        description: "Lots of information about Pokeman's favorite habitats and where you can find them",
        image: require('../assets/img/bulbizarre.png'),
    },
    {
        id: '4',
        title: 'Catching Pokeman',
        description: 'You can sign in add catch Pokeman, become a Pokeman lover',
        image: require('../assets/img/brasegali.png'),
    },
]

export default function WelcomeScreen({ navigation }) {
    // Image de fond
    const pokeball = require('../assets/img/pokeball.png');

    return (
      <View style={styles.container}>
          <ImageBackground source={pokeball} opacity={0.2} resizeMode='contain' style={styles.imageBg} />
          <Carrousel slides={slides} navigation={navigation} />       
          
          <View style={styles.bottomNav}>
              <ActionText onPress={() => navigation.navigate('SignIn')} title="Already account?" />
          </View>
      </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageBg: {
        position: 'absolute',
        top: -200,
        left: '5%',
        bottom: 0,
        right: 0,
        width: '95%',
    },
    bottomNav: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
})
