
/*
 * Screen pour l'inscription d'un client
 */

import { View, Text, StyleSheet } from 'react-native';
import { Colors } from '../assets/Colors';
import Input from '../components/Input';
import ActionButton from '../components/ActionButton';
import ActionText from '../components/ActionText';
import {useState} from 'react';
import {auth} from '../../firebase-config';

export default function SignUpScreen({ navigation }) {

  // Entrées pour le formulaire
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');

  const handleConnect = async() => {
    await navigation.navigate("Start");
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Sign up</Text>
      <View style={styles.form}>
        <Input 
          placeholder="Email"
          icon="email"
          onChangeText={text => setEmail(text)}
        />
        <Input
          placeholder="Name" 
          icon="account" 
          onChangeText={text=> setName(text)}
        />
        <Input
          placeholder="Password"
          secureTextEntry={true}
          icon="form-textbox-password"
          onChangeText={text => setPassword(text)}
        />
        <ActionButton
          title="Connect"
          onPress={handleConnect}
        />
      </View>

      <ActionText title="Already account?" onPress={() => navigation.navigate("SignIn")}/>

    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    fontWeight: 'bold',
    fontSize: 36,
    color: Colors.black,
    marginBottom: 100,
  },
  form: {
    width: 300,
  },
});
