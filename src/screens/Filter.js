/*
 * Screen pour le filtre de recherche
 */

import { View, Text, StyleSheet } from 'react-native';

export default function FilterScreen() {
  return (
    <View style={styles.container}>
      <Text>Filter</Text>
      </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
})
