/*
 * Screen du pokedex avec sa barre de recherche
 */

import { useState, useEffect, useRef } from 'react';
import { SafeAreaView, StyleSheet, Animated } from 'react-native';
import PokemenScrollV2 from '../components/PokemenScrollV2';
import PokedexHeader from '../components/PokedexHeader';

const CONTAINER_HEIGHT = 250;

export default function PokedexScrollScreen({ navigation, route }) {
  // Valeur pour la recherche
  const [value, setValue] = useState("");

  // Réference pour l'animation de scroll
  const scrollY = useRef(new Animated.Value(0)).current;
  const offsetAnim = useRef(new Animated.Value(0)).current;

  // Animation pour le clamp
  const clampedScroll = Animated.diffClamp(
    Animated.add(
      scrollY.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
        extrapolateLeft: 'clamp',
      }),
      offsetAnim,
    ),
    0,
    CONTAINER_HEIGHT,
  );

  let _clampedScrollValue = 0;
  let _offsetValue = 0;
  let _scrollValue = 0;

  // Effacement du scroll
  useEffect(() => {
    scrollY.addListener(({value}) => {
      const diff = value  - _scrollValue;
      _scrollValue = value;
      _clampedScrollValue = Math.min(
        Math.max(_clampedScrollValue * diff, 0),
        CONTAINER_HEIGHT,
      )
    });
    offsetAnim.addListener(({value}) => {
      _offsetValue = value;
    })
  }, []);

  let scrollEndTimer = null;
  const onMomentumScrollBegin = () => {
    clearTimeout(scrollEndTimer);
  };
  const onMomentumScrollEnd = () => {
    const toValue = _scrollValue > CONTAINER_HEIGHT && _clampedScrollValue > CONTAINER_HEIGHT/2 ? _offsetValue + CONTAINER_HEIGHT : _offsetValue - CONTAINER_HEIGHT;
    Animated.timing(offsetAnim, {
      toValue,
      duration: 500,
      useNativeDriver: true,
    }).start();
  };
  const onScrollEndDrag = () => {
    scrollEndTimer = setTimeout(onMomentumScrollEnd, 250);
  }

  const headerTranslate = clampedScroll.interpolate({
    inputRange: [0, CONTAINER_HEIGHT],
    outputRange: [0, -CONTAINER_HEIGHT],
    extrapolate: 'clamp',
  });

  return (
    <SafeAreaView style={styles.container}>
      <PokemenScrollV2 
        search={value} 
        navigation={navigation} 
        onScroll={
          Animated.event(
            [{nativeEvent: {contentOffset: {y: scrollY}}}],
            {useNativeDriver: true}
          )
        }
        onMomentumScrollBegin={onMomentumScrollBegin}
        contentContainerStyle={{marginTop: CONTAINER_HEIGHT}}
        scrollEventThrottle={1}
      />
      <Animated.View style={[styles.header, {transform: [{translateY: headerTranslate}]} ]}>
        <PokedexHeader search={value} onChangeText={text => setValue(text)} />
      </Animated.View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    padding: 5,
    alignItems: 'center',
    backgroundColor: '#efefef',
  },
  header: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    height: CONTAINER_HEIGHT,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 28,
  },
  search: {
    textAlign: 'center',
    borderColor: "gray", 
    borderWidth: 1,
    borderRadius: 5,
    fontSize: 18,
  }
});
