/*
 * Screen pour afficher la chaine d'evolution
 */
import { useContext } from 'react';
import {View, Text, StyleSheet} from 'react-native';
import PokemanContext from '../assets/Context';
import useEvolutionUrl from '../hooks/UseEvolutionUrl';
import EvolutionChart from '../components/EvolutionChart'; 

export default function EvolutionScreen() {
  const pokeman = useContext(PokemanContext);
  const [evolutionUrl, isLoading] = useEvolutionUrl(pokeman.id);

  return (
    <View style={styles.container}>
      {isLoading ? <Text>Loading</Text>: (
        <EvolutionChart evolutionUrl={evolutionUrl} />
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  h2: {
    fontWeight: 'bold',
    fontSize: 24,
  }
})
