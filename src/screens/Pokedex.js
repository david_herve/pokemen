/*
 * Screen du pokedex avec sa navigation
 */

import {createStackNavigator } from '@react-navigation/stack';
import { ApolloClient, InMemoryCache, ApolloProvider } from '@apollo/client';
import PokedexScrollScreen from './PokedexScroll';
import PokemanScreen from './Pokeman';
import GenerationsScreen from './Generations';
import FilterScreen from './Filter';
import SettingsScreen from './Settings';

const Stack = createStackNavigator();

// Wrap ApolloClient pour la recherche de pokemon
const client = new ApolloClient({
  uri: "https://beta.pokeapi.co/graphql/v1beta",
  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          pokemen: {
            merge: (existing = [], incoming, { args }) => {
              // On initial load or when adding a recipe, offset is 0 and only take the incoming data to avoid duplication
              if (args.offset == 0) {
                return [...incoming];
              }
              // This is only for pagination
              return [...existing, ...incoming];
            },
            read(existing, {
              args: {
                // Default to returning the entire cached list,
                // if offset and limit are not provided.
                offset = 0,
                limit = existing?.length,
              } = {},
            }) {
              return existing && existing.slice(offset, offset + limit);
            },
          }
        }
      }
    }
  }),
  onError: ({ networkError, graphQLErrors }) => {
    console.error("graphQLErrors", graphQLErrors)
    console.error("networkError", networkError)
  },
});

export default function PokedexScreen() {
  return (
    <ApolloProvider client={client} >
      <Stack.Navigator>
        <Stack.Screen
          name="PokedexScroll"
          component={PokedexScrollScreen} 
          options={{ headerShown: false }} 
        />
        <Stack.Group screenOptions={{ presentation: 'modal' }} >
          <Stack.Screen
            name="Pokeman" 
            component={PokemanScreen}
            options={{ headerShown: false }} 
          />
          <Stack.Screen
            name="Generations"
            component={GenerationsScreen}
            options={{ headerShown: false }} 
          />
          <Stack.Screen
            name="Filter"
            component={FilterScreen}
            options={{ headerShown: false }} 
          />
          <Stack.Screen
            name="Settings"
            component={SettingsScreen}
            options={{ headerShown: false }} 
          />
        </Stack.Group>
      </Stack.Navigator>
    </ApolloProvider >
  );
}
