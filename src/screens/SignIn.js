/*
 * Screen pour la connexion d'un client
 */

import { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Colors } from '../assets/Colors';
import ActionButton from '../components/ActionButton';
import ActionText from '../components/ActionText';
import Input from '../components/Input';

export default function SignInScreen({ navigation }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Sign In</Text>
      <View style={styles.form}>
        <Input 
          placeholder="Email" 
          icon="email" 
          onChangeText={text => setEmail(text)}
        />
        <Input 
          placeholder="Password" 
          secureTextEntry={true} 
          icon="form-textbox-password"
          onChangeText={text => setPassword(text)}
        />
        <ActionButton title="Log in" onPress={() => navigation.navigate("Start")} />
      </View>
      <ActionText title="Forgot password?" />
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    fontWeight: 'bold',
    fontSize: 36,
    color: Colors.black,
  },
  form: {
    width: 300,
  },
})
