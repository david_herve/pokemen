/*
 * Screen pour le filtre de génération
 */

import React, {useState, useEffect} from 'react';
import { NavigationContext } from "@react-navigation/native";
import { View, Text, StyleSheet, FlatList } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { Colors } from '../assets/Colors';
import Generation from '../components/Generation';
import useGenerations from '../hooks/UseGenerations';

// Formatage des données pour une liste
const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);

  let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
  while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
    data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
    numberOfElementsLastRow++;
  }

  return data;
};

const numColumns = 2;

export default function GenerationsScreen() {

  // Etats du screen
  const [generations, isLoading] = useGenerations();
  const navigation = React.useContext(NavigationContext);
  
  // Rendu d'une carde dans la flatList
  const renderItem = ({ item }) => {
    if (item.empty === true) {
      return <Text>Empty</Text>;
    }
    return (
      <Generation name={item.name} url={item.url} />
    );
  };

  // Id unique pour la generations
  const keyExtractor = (item) => {
    return item.name;
  };

  return (
    <View style={styles.container}>
      <Ionicons style={styles.back} name="arrow-back" size={33} color="white" onPress={() => navigation.navigate('PokedexScroll')} />
      <View style={styles.card}>
        <Text style={styles.title}>Generations</Text>
        <Text style={styles.subtitle}>Use search for generations to explore your Pokémon!</Text>
        {isLoading ? <Text>Loading</Text> :
        <FlatList
          data={formatData(generations, numColumns)}
          renderItem={renderItem}
          keyExtractor={keyExtractor}
          style={styles.list}
          numColumns={numColumns}
        />
        }
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.foreground,
  },
  back: {
    position: 'absolute',
    top: 50,
    left: 10,
    bottom: 0,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 28,
    textAlign: 'left',
  },
  subtitle: {},
  card: {
    position: 'absolute',
    zIndex: -1,
    top: 200,
    bottom: 0,
    left: 0,
    right: 0,
    width: '100%',
    height: '100%', 
    backgroundColor: Colors.white,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    padding: 25,
    height: '80%',
  },
  list: {
    flex: 1,
    marginVertical: 20,
  }
})
