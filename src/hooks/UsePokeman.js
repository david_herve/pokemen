/*
 * Hooker personnalisé pour la récupération d'un pokeman depuis son id ou son nom
 */
import { useState, useEffect } from 'react';
import {nameFormat, idFormat, getColorAndIconByType} from '../assets/Utils';

const url = "https://pokeapi.co/api/v2/pokemon/";

export default function usePokeman(name) {
  // Les états sont l'objet pokeman et isLoading pour l'asynchrone du hooker
  const [isLoading, setLoading] = useState(true);
  const [pokeman, setPokeman] = useState({});

  // Récupération asynchrone du pokemon via l'api pokeapi
  // (voir l'appel : https://pokeapi.co/api/v2/pokemon/?limit=20&offset=0 )
  const getPokemen = async() => {
    try {
      const response = 
        await fetch(url + name);
      return await response.json();

    } catch (error) {
      console.error(error);
    }
  }

  // Chargement du pokeman pendant le changement d'état du composant avec annulation
  useEffect(() => {
    let isCancelled = false;

    getPokemen().then((json) => {
      if (!isCancelled) {

        /* Assignement dans la variable pokeman : 
         *  - ajout du json
         *  - ajout de l'id en string
         *  - ajout de l'idNum en integer
         *  - ajout de la couleur principal (selon le premier type)
         */
        setPokeman({
          ...json,
          nameFormat: nameFormat(json.name),
          idFormat: idFormat(json.id),
          color: getColorAndIconByType(json.types[0].type.name).color,
        });

        // Finir le chargement
        setLoading(false);
      }
    });

    return () => isCancelled = true;
  }, []);

  // Retourner pokeman et isLoading (important: car appel asynchrone)
  return [pokeman, isLoading];

}
