/*
 * Hook personnalisé pour obtenir la liste des pokemons par generation
 */

import { useState, useEffect } from "react";

const url = 'https://pokeapi.co/api/v2/generation?limit=10';

export default function useGenerations() {
  // Les états sont l'objet generations et isLoading pour l'asynchrone du hooker
  const [isLoading, setLoading] = useState(true);
  const [generations, setGenerations] = useState([]);

  // Récupération asynchrone des generations via l'api pokeapi
  // (voir l'appel : https://pokeapi.co/api/v2/generation?limit=10
  const getGenerations = async() => {
    try {
      const response = 
        await fetch(url);
      return await response.json();

    } catch (error) {
      console.error(error);
    }
  }

  // Chargement des generations pendant le changement d'état du composant avec annulation
  useEffect(() => {
    let isCancelled = false;

    getGenerations().then((json) => {
      if (!isCancelled) {

        // Assignement dans la variable generations: 
        setGenerations([...json.results]);

        // Finir le chargement
        setLoading(false);
      }
    });

    return () => isCancelled = true;
  }, []);

  // Retourner pokeman et isLoading (important: car appel asynchrone)
  return [generations, isLoading];

}
