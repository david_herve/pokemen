/*
 * Hook personnalisé pour les generations d'un pokemon
 */
import { useState, useEffect } from "react";

export default function useGeneration(url) {
  // Les états sont l'objet generations et isLoading pour l'asynchrone du hooker
  const [isLoading, setLoading] = useState(true);
  const [generation, setGeneration] = useState([]);

  // Récupération asynchrone des generations via l'api pokeapi
  // (voir l'appel : https://pokeapi.co/api/v2/generation?limit=10
  const getGeneration = async() => {
    try {
      const response = 
        await fetch(url);
      return await response.json();

    } catch (error) {
      console.error(error);
    }
  }

  // Chargement des generations pendant le changement d'état du composant avec annulation
  useEffect(() => {
    let isCancelled = false;

    getGeneration().then((json) => {
      if (!isCancelled) {

        // Assignement dans la variable generation: 
        setGeneration(json.pokemon_species.slice(0, 3))

        // Finir le chargement
        setLoading(false);
      }
    });

    return () => isCancelled = true;
  }, []);

  // Retourner pokeman et isLoading (important: car appel asynchrone)
  return [generation, isLoading];

}
