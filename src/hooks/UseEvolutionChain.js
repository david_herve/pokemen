/*
 * Hook personnalisé pour récupérer une liste de pokemon dans la chaine d'evolution
 */
import { useState, useEffect } from "react";

export default function useEvolutionChain(url) {
  
  // Les etats sont pokemenChain et isLoading (controle pour appel asynchrone)
  const [isLoading, setLoading] = useState(true);
  const [pokemenChain, setPokemenChain] = useState([]);

  // Appel asynchrone sur pokemon-species
  const getChain = async() => {
    try {
      const response = 
        await fetch(url);
      const json = await response.json();

      // Parcours de la chaine et formattage d'une ligne d'evolution ( a => b )
      let seed = json.chain;
      let index = 0;
      const chain = [];
      while(seed.evolves_to[0]) {
        chain.push({
          start: seed.species,
          end: seed.evolves_to[0].species,
          level: seed.evolves_to[0].evolution_details[0].min_level,
          index,
        });
        seed = seed.evolves_to[0];
        index++;
      }

      setPokemenChain(chain);

    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  // Utilisation d'un hook automatique
  useEffect(() => {
    getChain();
  }, []);

  // Retourner pokemenChain et isLoading pour le controle asynchrone
  return [pokemenChain, isLoading];

}
