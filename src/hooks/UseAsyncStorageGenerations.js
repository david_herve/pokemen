/*
 * Hook personnalisé pour l'utilisation d'un AsyncStorage pour @generations (list)
 */

import { useState, useEffect } from "react";
import AsyncStorage from '@react-native-async-storage/async-storage';


export default function useAsyncStorageGenerations() {

  // Liste qui est un état
  const [generations, setGenerations] = useState([]);

  // AsyncStorage pour @generation
  const storeGenerations = async(list) => {
    try {
      await AsyncStorage.setItem('@generations', JSON.stringify(list));
    } catch (e) {
      console.error(e);
    }
  }

  const getGenerations = async() => {
    try {
      return await AsyncStorage.getItem('@generations');
    } catch(e) {
      console.error(e);
    }
  }

  // Chargement automatique dans le localStorage du @generations
  useEffect(() => {
    let isCancelled = false;

    getGenerations().then((list) => {
      if (!isCancelled && list !== null) {
        setGenerations(JSON.parse(list));
      }
    });

    return () => isCancelled = true;
  }, []);

  return [generations, {storeGenerations, getGenerations}];
}
