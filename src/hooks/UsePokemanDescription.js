/*
 * Hook personnalisé pour récupérer la decription d'un pokeman
 */
import { useState, useEffect } from "react";

const url = "https://pokeapi.co/api/v2/pokemon-species/"

export default function usePokemanDescription(id) {
  
  // Les etats sont description et isLoading (controle pour appel asynchrone)
  const [isLoading, setLoading] = useState(true);
  const [description, setDescription] = useState([]);

  // Appel asynchrone sur pokemon-species
  const getDescription = async() => {
    try {
      const response = 
        await fetch(url + id);
      const json = await response.json();

      // Modification de la description
      setDescription(json.flavor_text_entries[0].flavor_text)

    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  // Utilisation d'un hook pour la récupération des attributs du pokeman et de son nettoyage
  useEffect(() => {
    getDescription();
  }, []);

  // Retourner la description et isLoading pour le controle asynchrone
  return [description, isLoading];

}
