/*
 * Hook personnalisé pour récupérer la chaine d'evolution pour un pokemon
 */
import { useState, useEffect } from "react";

const url = "https://pokeapi.co/api/v2/pokemon-species/"

export default function useEvolutionUrl(id) {
  
  // Les etats sont evolutionUrl et isLoading (controle pour appel asynchrone)
  const [isLoading, setLoading] = useState(true);
  const [evolutionUrl, setEvolutionUrl] = useState([]);

  // Appel asynchrone sur pokemon-species
  const getEvolutionUrl = async() => {
    try {
      const response = 
        await fetch(url + id);
      const json = await response.json();

      // Modification de evolutionUrl 
      setEvolutionUrl(json.evolution_chain.url);

    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  // Utilisation d'un hook pour le chargement automatique
  useEffect(() => getEvolutionUrl(), []);

  // Retourner la evolutionUrl et isLoading pour le controle asynchrone
  return [evolutionUrl, isLoading];

}
