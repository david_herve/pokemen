/*
 * Hook personnalisé pour faire une requete
 */

import { useState, useEffect } from "react";
import { useQuery } from "@apollo/client";
import { SEARCH_POKEMON_BY_ID, SEARCH_POKEMON_BY_NAME, SEARCH_POKEMON_BY_ID_AND_GENERATIONS, SEARCH_POKEMON_BY_NAME_AND_GENERATIONS} from "../assets/queries/searchPokeman";
import AsyncStorage from '@react-native-async-storage/async-storage';

// Utilisaiton de async storage pour les filtres de recherche
const getGenerations = async() => {
  try {
    return await AsyncStorage.getItem('@generations');
  } catch(e) {
    console.error(e);
  }
}

export default function usePokeApiQuery(search) {
  // Limite pour le nombre de pokemon
  const [limit, setLimit] = useState(898);

  // Utilisation du filtre generations
  const [generations, setGenerations] = useState([]);

  // Chargement a l'appel du hook
  useEffect(() => {
    let isCancelled = false;

    getGenerations().then((list) => {
      if (!isCancelled && list !== null) {
        const gen = JSON.parse(list);
        setGenerations(gen);
      }
    });

    return () => isCancelled = true;
  }, [])

  // Function pour changer la requete selon le type de search
  function getQuery(value) {
    
   // if(generations) {

   //   if(value === "") {
   //     return useQuery(
   //       SEARCH_POKEMON_BY_NAME_AND_GENERATIONS,
   //       {
   //         variables: {generations, pokeman: value + "(?!.*-)", offset: 0, limit},
   //       } 
   //     )
   //   }

   //   if(isNaN(value)) {
   //     return useQuery(
   //       SEARCH_POKEMON_BY_NAME_AND_GENERATIONS,
   //       {
   //         variables: {generations, pokeman: value + "(?!.*-)", offset: 0, limit},
   //       } 
   //     )
   //   }

   //   return useQuery(
   //     SEARCH_POKEMON_BY_ID_AND_GENERATIONS,
   //     {
   //       variables: {generations, id: parseInt(search), offset: 0, limit},
   //     }
   //   )

   // }else {

      if(value === "") {
        return useQuery(
          SEARCH_POKEMON_BY_NAME,
          {
            variables: {pokeman: value + "(?!.*-)", offset: 0, limit},
          } 
        )
      }

      if(isNaN(value)) {
        return useQuery(
          SEARCH_POKEMON_BY_NAME,
          {
            variables: {pokeman: value + "(?!.*-)", offset: 0, limit},
          } 
        )
      }

      return useQuery(
        SEARCH_POKEMON_BY_ID,
        {
          variables: {id: parseInt(search), offset: 0, limit},
        }
      )

  }

  // Recuperation pour la requete
  const { loading, data, fetchMore, refetch } = getQuery(search);

  // Chargement de plus de contenu la fin de la liste
  const onLoadMore = async () => {
    const currentLength = data.pokemen.length;
    fetchMore({
      variables: {
        offset: currentLength,
        limit: 20,
      }
    }).then(fetchMoreResult => {
      setLimit(currentLength + fetchMoreResult.data.pokemen.length);
    });
  };

  return {loading, refetch, data, onLoadMore}

}
