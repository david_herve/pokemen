/*
 * Contexte pour d'un pokemon pour faire passer des props dans le composant enfant
 */
import { createContext } from 'react'

const PokemanContext = createContext();

export default PokemanContext;
