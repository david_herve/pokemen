// Fonction utilitaire pour obtenir la couleur et l'icon selon le type
module.exports.getColorAndIconByType = (type) => {
  switch(type) {
    case "normal":
      return {color: "#ABABAB", icon: "radiobox-marked", lib: "materialcommunityicons"};
    case "fighting":
      return {color: "#C03028", icon: "boxing-glove", lib: "materialcommunityicons"};
    case "flying":
      return {color: "#A890F0", icon: "air", lib: "entypo"};
    case "poison":
      return {color: "#8441A3", icon: "bottle-tonic-skull", lib: "materialcommunityicons"};
    case "ground":
      return {color: "#F99668", icon: "filter-hdr", lib: "materialicons"};
    case "rock":
      return {color: "#B8A038", icon: "bullseye", lib: "materialcommunityicons"};
    case "bug":
      return {color: "#6FAB5C", icon: "ladybug", lib: "materialcommunityicons"};
    case "ghost":
      return {color: "#705898", icon: "ghost", lib: "materialcommunityicons"};
    case "steel":
      return {color: "#B8B8D0", icon: "hexagon-slice-6", lib: "materialcommunityicons"};
    case "fire":
      return {color: "#F08030", icon: "fire", lib: "materialcommunityicons"};
    case "water":
      return {color: "#6890F0", icon: "water", lib: "materialcommunityicons"};
    case "grass":
      return {color: "#8BBE8A", icon: "grass", lib: "materialicons"};
    case "electric":
      return {color: "#F2CB55", icon: "offline-bolt", lib: "materialicons"};
    case "psychic":
      return {color: "#F85888", icon: "eye", lib: "fontawesome"};
    case "ice":
      return {color: "#98D8D8", icon: "snowflake-o", lib: "fontawesome"};
    case "dragon":
      return {color: "#7038F8", icon: "dragon", lib: "fontawesome5"};
    case "dark":
      return {color: "#705848", icon: "moon-sharp", lib: "ionicons"};
    case "fairy":
      return {color: "#FFAEC9", icon: "magic", lib: "fontawesome"};
    case "unknown":
      return {color: "#68A090", icon: "help", lib: "entypo"};
    case "shadow":
      return {color: "#425165", icon: "box-shadow", lib: "materialcommunityicons"};
    default:
      return {color: "#68A090", icon: "help", lib: "entypo"};
  };
};

// Fonction utilitaire pour le formattage du nom : Nom
module.exports.nameFormat = (name) => {
  return name.charAt(0).toUpperCase() + name.slice(1);
}

// Fonction utilitaire pour le formattage de l'id : #001
module.exports.idFormat = (id) => {
  if(id < 10){
    return '#00' + id;
  }else{
    return '#' + ('0' + id).slice(-3);
  }
}

// Fonction utilitaire pour obtenir une couleur plus claire selon un pourcentage
module.exports.lightenColor = (color, percent) => {
  const num = parseInt(color.replace("#",""),16);
  const amt = Math.round(2.55 * percent);
  const R = (num >> 16) + amt;
  const B = (num >> 8 & 0x00FF) + amt;
  const G = (num & 0x0000FF) + amt;
  return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
};

// Fonction utilitaire pour obtenir une couleur plus sombre selon un pourcentage
module.exports.darkenColor = (color, percent) => {
  const num = parseInt(color.replace("#",""),16);
  const amt = Math.round(2.55 * -percent);
  const R = (num >> 16) + amt;
  const B = (num >> 8 & 0x00FF) + amt;
  const G = (num & 0x0000FF) + amt;
  return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
};
