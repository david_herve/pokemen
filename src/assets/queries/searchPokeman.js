/*
 * Requetes pour la recherche d'un pokemon
 */
import { gql } from '@apollo/client';

const SEARCH_POKEMON_BY_NAME = gql`
  query Query($pokeman: String!, $offset: Int!, $limit: Int!) {
    pokemen: pokemon_v2_pokemon(where: {name: {_regex: $pokeman}}, order_by: {id: asc}, offset: $offset, limit: $limit) {
      id
      name
    }
  }
`;

const SEARCH_POKEMON_BY_ID = gql`
  query Query($id: Int!, $offset: Int!, $limit: Int!) {
    pokemen: pokemon_v2_pokemon(where: {id: {_eq: $id}}, order_by: {id: asc}, offset: $offset, limit: $limit) {
      id
      name
    }
  }
`;

const SEARCH_POKEMON_BY_NAME_AND_GENERATIONS = gql`
  query Query($generations: [String]!, $pokeman: String!, $offset: Int!, $limit: Int!) {
    pokemen: pokemon_v2_pokemonspecies(where: {pokemon_v2_generation: {name: {_in: $generations}}, name: {_regex: $pokeman}}, order_by: {id: asc}, offset: $offset, limit: $limit) {
      id
      name
    }
  }
`
const SEARCH_POKEMON_BY_ID_AND_GENERATIONS = gql`
  query Query($generations: [String]!, $id: Int!, $offset: Int!, $limit: Int!) {
    pokemen: pokemon_v2_pokemonspecies(where: {pokemon_v2_generation: {name: {_in: $generations}}, id: {_eq: $id}}, order_by: {id: asc}, offset: $offset, limit: $limit) {
      id
      name
    }
  }
`

export {SEARCH_POKEMON_BY_NAME, SEARCH_POKEMON_BY_ID, SEARCH_POKEMON_BY_NAME_AND_GENERATIONS, SEARCH_POKEMON_BY_ID_AND_GENERATIONS}
