/*
 * Palette de couleurs de la maquette
 */

export const Colors = {
    background: "#efefef",
    foreground: "#f7846c",
    white: "#ffffff",
    black: "#353535",
    darkGrey: "#757474",
    softGrey: "#959595",
}
