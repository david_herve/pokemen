# Projet React Native : Pokemen

## Ressources 

Carrousel : 

https://www.youtube.com/watch?v=r2NJJye0XnM

Maquettes : 

https://www.behance.net/gallery/121956993/Pokedex-Mobile-Application?tracking_source=search_projects_recommended%7Cpokedex 

https://www.behance.net/gallery/95727849/Pokdex-App?tracking_source=search_projects%7CPokedex

Api :

Utilisation de PokeApi avec la beta du GraphQL.

https://pokeapi.co/

## EXPO

Le projet est sous EXPO, lancé la commande suivante pour démarrer le projet:

```
yarn start

#ou

npm run start
```

Suivez les instructions du cli.
Parfois le projet doit être en mode tunnel pour se lancer.

## Fonctionnalitées

1. navigation entre screens :
  a. par une top navbar (ex: screen de login);
  b. par une bottom navbar (ex: screen de start);
  c. par bouton.
2. utilisation de contexte pour une eviter le "prop drilling";
3. implementation d'une recherche par requête sur base de données;
4. réalisation d'une animation pour se sensibiliser à l'expérience utilisateur;
5. présentation du pokemon avec ces statistiques et sa chaine d'évolution;
6. respect des maquettes.

## A améliorer ou faire

1. réduire les appels api;
2. réaliser les filtres de recherche; 
3. implementation des screens région et profile;
4. faire un système d'authenfication (firebase étant limité sous expo);
5. ajouter les pokemons dans le profile;
6. faire l'internatilisation de l'application.

